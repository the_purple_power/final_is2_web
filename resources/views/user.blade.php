@extends('layouts.main')

@section('content')
    {{--<div class="row center-align">--}}
        {{--<div class="row container">--}}
                {{--<div class="col s8">--}}
                {{--<h4>Employee # {{$emp->emp_no}} </h4>--}}
                {{--<h3>{{$emp->first_name}} {{$emp->last_name}}</h3>--}}
            {{--</div>--}}
            {{--<div class="col s4">--}}
                {{--<span style="font-size:10em" class="material-icons">person</span>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row container">--}}
            {{--<div class="col s6">--}}
                {{--<h5>Department: {{$emp->departmentNow()->dept_name or 'fired'}}</h5>--}}
            {{--</div>--}}
            {{--   Department:{{$info[0]}}--}}
               {{--Salary:{{$info[1]}}--}}
               {{--Title:{{$info[2]}}--}}
               {{--Manager : {{$info[3]}}--}}
               {{--Name:{{$emp->first_name}}--}}

        {{--</div>--}}
    {{--</div>--}}
    <input id="toD" type="hidden" name="toDate" value="{{$salary->last()->to_date}}">
    <div class="container">
        <div class="row">
            <div class="col m9 offset-m1">
                <div class="card-panel blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="row">
                            <div class="col m8 offset-m1">
                                <span class="card-title"><h4>Employee #{{$emp->emp_no}}</h4></span>
                            </div>
                            <div class="col m3">
                                <i class="large material-icons">account_box</i>
                            </div>
                        </div>
                        <div class="divider "></div>
                        <div class="row">
                            <div class="col m6 offset-m1">
                                <p>Name: {{$emp->first_name}}</p>

                                <p>SecondName : {{$emp->last_name}}</p>
                                <p>Salary:{{$salary->last()->salary}}</p>
                                <p>Department:{{$dep->dept_name}}</p>
                                <p>Title: {{$titles->last()->title}}</p>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Graphics:--}}
    <div class="container">
        <div class="row">
        <div class="col m12 l6">
        <canvas id="myChart" width="400" height="300"></canvas>
        </div>
            <div class="col m12 l6">
                <canvas id="myC" width="400" height="300"></canvas>
            </div>
        </div>
    </div>


    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var toD = $('#toD').val();
        toD = toD.split('-');
         if(toD[0]>8000)
             toD ="now";

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                {{--//labels: [{{$emp->hire_date}}, "Blue", "Yellow", "Green", "Purple", "Orange"],--}}
                labels:[
                    @foreach($salary as $s)
                {{$s->from_date}},
                    @endforeach
                    toD,
                ],
                datasets: [{
                    label: '#changing the Salary',
                    //data: [9, 19, 3, 5, 2, 3],
                    data:[
                        @foreach($salary as $s)
                        {{$s->salary}},
                        @endforeach
                        {{$salary->last()->salary}}
                    ],
                    backgroundColor: '#18ffff',

                }]
            },
            options: {
                dutartion:0,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:false
                        }
                    }]
                },
                hover: {
                    animationDuration: 2, // duration of animations when hovering an item
                },
            }
        });

        var ctx = document.getElementById("myC").getContext('2d');
        var myC = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels:[
                    @foreach($titles as $t)
                   "{{$t->title}}",
                    @endforeach
                ],

                datasets: [{
                                                                                                                                                                                                                                                                                                                                                                                                                                                //data: [9, 19, 3, 5, 2, 3],
                    data: [
                        @foreach($titles as $t)
                        "{{$t->to_date - $t->from_date}}",
                        @endforeach

                    ],
                    backgroundColor: [
            '#d32f2f',
            '#7b1fa2',
            '#3949ab',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ],

                }]
            },

        });
    </script>




@endsection