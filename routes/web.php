<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexGraphsController@index')->name('/');
Route::get('/fluidity', 'IndexGraphsController@fluidity')->name('fluidity')->middleware('auth');
Route::get('/avg_gender', 'IndexGraphsController@avgGender')->name('avg_gender')->middleware('auth');
Route::get('/avg_title', 'IndexGraphsController@avgTitle')->name('avg_title')->middleware('auth');
Route::get('/avg_year', 'IndexGraphsController@avgYear')->name('avg_year')->middleware('auth');
Route::get('/avg_age', 'IndexGraphsController@avgAge')->name('avg_age')->middleware('auth');
Route::get('/num_depts', 'IndexGraphsController@numDept')->name('num_depts')->middleware('auth');
Route::get('/num_genders', 'IndexGraphsController@numGenders')->name('num_genders')->middleware('auth');

Route::get('/tst', function () {
    return view('home');
});

Route::get('/employees', 'EmpController@show')->name('employee')->middleware('auth');
Route::get('/employees/get', 'EmpController@get')->name('empTable')->middleware('auth');
Route::get('/department', 'DepController@show')->name('department')->middleware('auth');
Route::get('/department/get', 'DepController@get')->name('depTable')->middleware('auth');
Route::get('/title', 'TitleController@show')->name('title')->middleware('auth');
Route::get('/title/get', 'TitleController@get')->name('titleTable')->middleware('auth');
Auth::routes();

Route::get('/employee/{id}', 'UserInfoController@userInfo')->name('userInfo')->middleware('auth');//employee info