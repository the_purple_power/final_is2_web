
function setOrder(order) {
    var col = $('#orderColumn');
    var sort = $('#sortOrder');
    var arrow = $('#' + order + ' .material-icons');
    var form = $('#sortForm');
    if (col.val() === order) {
        if (sort.val() === 'asc') {
            sort.val('desc');
            arrow.text('keyboard_arrow_down');
            form.submit();
        }
        else {
            sort.val('asc');
            arrow.text('keyboard_arrow_up');
            form.submit();
        }
    }
    else {
        col.val(order);
        sort.val('asc');
        var icons=$('#trow th a .material-icons');
        icons.removeClass('hide');
        icons.addClass('hide');
        arrow.removeClass('hide');
        arrow.text('keyboard_arrow_up');
        form.submit();
    }
    // console.log('new val: '+col.val()+'/'+sort.val());
}
//for AJAX
/*function setOrder(order) {
    var col = $('#orderColumn');
    var sort = $('#sortOrder');
    var arrow = $('#' + order + ' .material-icons');
    if (col.val() === order) {
        if (sort.val() === 'asc') {
            sort.val('desc');
            arrow.text('keyboard_arrow_down');
        }
        else {
            sort.val('asc');
            arrow.text('keyboard_arrow_up');
        }
    }
    else {
        col.val(order);
        sort.val('asc');
        var icons=$('#trow th a .material-icons');
        icons.removeClass('hide');
        icons.addClass('hide');
        arrow.removeClass('hide');
        arrow.text('keyboard_arrow_up');

    }
    // console.log('new val: '+col.val()+'/'+sort.val());
}*/

