<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('salaries2', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_no');
            $table->integer('salary');
            $table->date('from_date');
            $table->date('to_date');
        });

        DB::statement("INSERT INTO salaries2 (emp_no,salary,from_date,to_date) SELECT emp_no,salary,from_date,to_date FROM salaries;");
        Schema::dropIfExists('salaries');
        Schema::rename('salaries2', 'salaries');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries2');
    }
}
