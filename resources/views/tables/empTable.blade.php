@if($result->count()==0)
    <h4>No results</h4>
@else
    <p>Total: {{$result->total()}}</p>
    <table class=" centered">
        <thead>
        <tr id="trow">
            {{--<span class="material-icons">keyboard_arrow_up</span>--}}
            <th><a id="emp_no" href="#" onclick="setOrder('emp_no');"><span class="hide material-icons">keyboard_arrow_up</span>
                    Emp_no/info</a></th>
            <th><a id="first_name" href="#" onclick="setOrder('first_name');"><span
                            class="hide material-icons">keyboard_arrow_up</span>
                    First Name</a></th>
            <th><a id="last_name" href="#" onclick="setOrder('last_name');"><span
                            class="hide material-icons">keyboard_arrow_up</span>
                    Last Name</a></th>
            <th><a id="gender" href="#" onclick="setOrder('gender');"><span class="hide material-icons">keyboard_arrow_up</span>
                    Gender</a></th>
            <th><a id="salary" href="#" onclick="setOrder('salary');"><span class="hide material-icons">keyboard_arrow_up</span>
                    Salary</a></th>

            @if($isCurrent)
                <th><a id="hire_date" href="#" onclick="setOrder('hire_date');"><span
                                class="hide material-icons">keyboard_arrow_up</span>
                        Hire date</a></th>
            @else
                <th><a id="from_date" href="#" onclick="setOrder('from_date');"><span
                                class="hide material-icons">keyboard_arrow_up</span>
                        From</a></th>
                <th><a id="to_date" href="#" onclick="setOrder('to_date');"><span
                                class="hide material-icons">keyboard_arrow_up</span>
                        To</a></th>
            @endif
        </tr>
        </thead>

        <script>
            $(document).ready(function () {
                @if($order === 'asc')
                $('#{{$orderCol}} .material-icons').text('keyboard_arrow_up');
                @else
                $('#{{$orderCol}} .material-icons').text('keyboard_arrow_down');
                @endif
                $('#{{$orderCol}} .material-icons').removeClass('hide');
            });
        </script>
        <tbody>

        @if(!$isCurrent)
            @foreach($result as $emp )
                <tr>
                    <td><a href="{{route('userInfo',['id' => $emp->emp_no]) }}">{{$emp->emp_no}}</a></td>
                    <td><p>{{$emp->first_name}}</p></td>
                    <td><p>{{$emp->last_name}}</p></td>
                    <td><p>{{$emp->gender}}</p></td>
                    @if($orderCol !== 'salary'&& $orderCol !== 'from_date' && $orderCol !== 'to_date')
                        <td>
                            @foreach($emp->salaries()->cursor() as $sal )
                                <div>{{$sal->salary}} zł</div>
                            @endforeach
                        </td>
                        <td>
                            @foreach($emp->salaries()->cursor() as $sal )
                                <div>{{$sal->from_date}}</div>
                            @endforeach
                        </td>
                        <td>
                            @foreach($emp->salaries()->cursor() as $sal )
                                <div>{{$sal->to_date}}</div>
                            @endforeach
                        </td>
                    @else
                        <td><p>{{$emp->salary}}</p></td>  {{----}}
                        <td><p>{{$emp->from_date}}</p></td>
                        <td><p>{{$emp->to_date}}</p></td>
                    @endif
                </tr>
            @endforeach

        @else

            @foreach($result as $emp)
                <tr>
                    <td><a href="{{route('userInfo',['id' => $emp->emp_no]) }}">{{$emp->emp_no}}</a>
                    </td>
                    <td><p>{{$emp->first_name}}</p></td>
                    <td><p>{{$emp->last_name}}</p></td>
                    <td><p>{{$emp->gender}}</p></td>

                    @if($tableType)
                        @if($emp->salaryNow() ===null)
                            <td><p>Fired</p></td>
                        @else
                            <td><p>{{$emp->salaryNow()->salary}} zł</p></td>
                        @endif
                    @else
                        <td><p>{{$emp->salary}} zł</p></td>
                    @endif

                    <td>
                        <p>{{$emp->hire_date}}</p>
                    </td>
                </tr>
            @endforeach
        @endif

        </tbody>
    </table>
    {{$result->appends($params)->links()}}

@endif