<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model {
    protected $table = 'salaries';

    public function employee() {
        return $this->belongsTo(Employee::class, 'emp_no','emp_no');
    }

    public function titles() {
        return $this->belongsToMany(Title::class);
    }
//    $from and $to are already have default value
    public function scopeSalaryDate($query,$from, $to){
        return $query->where('from_date','>',$from)->where('to_date','<',$to);
    }
    public function scopeSalaryRange($query,$from,$to){
        return $query->whereBetween('salary',[$from,$to]);
    }
    public function scopeCurrentSalary($query)
    {
        return $query->where('to_date','>', date('Y-m-d', time()));
    }
}
