@extends('layouts.main')
@section('content')
    @php $par = collect($params); @endphp
    <div class="row center-align">
        <div class="col m12 l6">
            <div class="row">
                <div class="col m6 s12 valign-wrapper">
                    <div class="input-field col s6">
                        <input value="{{$par->get('emp')}}" name="emp" id="empSearch" type="text"
                               class="" form="sortForm" onclick="form.preventDefault()">
                        <label for="empSearch" class="material-icons">search</label>
                    </div>
                    <div class="col s6">
                        <button onclick="$('#empSearch').val('')" class="btn waves-effect waves-purple material-icons">
                            close
                        </button>
                    </div>
                </div>
                <div class="col m6 s12">
                    <h5>Current status</h5>
                    <div class="switch">
                        <label>
                            Off
                            <input type="checkbox" onclick="sortForm.submit();" name="current" form="sortForm"
                                   @if($isCurrent)
                                   checked
                                    @endif
                            >
                            <span class="lever"></span>
                            On
                        </label>
                    </div>
                </div>
            </div>
            <div id="empPLoader" class="progress white">
                <div class="indeterminate purple darken-3"></div>
            </div>
            <div id="empTable">

                {{-- results from AJAX--}}
            </div>
        </div>


        <script>
            var filterForm = $('#sortForm');
            var empPLoader = $('#empPLoader');
            var empTable = $('#empTable');

            $(document).ready(function () {
                getTable();


                filterForm.submit(function (event) {
                    event.preventDefault();
                    getTable()
                });

                $(document).on('click', '.pagination a', function (e) {
                    empPLoader.show();
                    e.preventDefault();
                    // console.log($(this).attr('href')/*.split('page=')[1]*/);
                    axios.get($(this).attr('href')).then(function (response) { //todo
                        empPLoader.hide();
                        empTable.html(response.data);

                    }).catch();
                });

            });

            function getTable() {
                empPLoader.show();
                console.log('loading...');
                var result = {};
                $.each(filterForm.serializeArray(), function () {
                    result[this.name] = this.value;
                });

                axios.get("{{route('empTable')}}", {params: result}).then(function (response) { //todo
                    empPLoader.hide();
                    empTable.html(response.data);

                }).catch();
            }
        </script>

        <div class="col m12 l6">
            <div class="section">
                <h2 class="center">Number of people by genders</h2>
                {{--SLOW--}}
                <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
                    <canvas id="num_genders"></canvas>
                </div>
            </div>
            {{--Total by dept--}}
            <script>
                var ctx = $("#num_genders");
                var myChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: [
                            'Male', 'Females'
                        ],
                        datasets: [{
                            data: [
                                @foreach($num_genders as $nd)
                                {{ $nd['total'] }},
                                @endforeach
                            ],
                            backgroundColor: [
                                'rgba(67,160,71,0.05)',
                                'rgba(54, 162, 235, 0.2)'
                            ],
                            borderColor: [
                                'rgb(67,160,71)',
                                'rgb(54, 162, 235)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        legend: {
                            display: true,
                            position: 'bottom'
                        }
                    }
                });
            </script>

        </div>
    </div>

@endsection