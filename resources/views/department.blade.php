@extends('layouts.main')
@section('content')
    @php
        $par = collect($params);
        $isCurrent =  collect($params)->get('current');

    @endphp
    <div class="row">
        <div class="col m6 sm12">
            <div class="input-field col s12 m6">
                {{--<form name="add" action="{{route('department')}}" method="get">--}}
                <select name="department" onchange="this.form.submit()" form="sortForm">
                    @if($par->get('department') ===null)
                        <option value="" disabled selected>Choose department</option>
                    @else
                        <option value="{{$par->get('department')}}" selected>{{$par->get('department')}}</option>

                    @endif
                    @foreach($dep as $d)
                        <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>
                    @endforeach

                </select>
                {{--</form>--}}
            </div>
            <div class="col m6 s12">
                <h5>Current status</h5>
                <div class="switch">
                    <label>
                        Off
                        <input type="checkbox" onclick="sortForm.submit();" name="current" form="sortForm"
                               @if($isCurrent)
                               checked
                                @endif
                        >
                        <span class="lever"></span>
                        On
                    </label>
                </div>
            </div>
        </div>
    </div>


    <div class="row center-align">
    <div class="col m12 l6">


        <div id="empPLoader" class="progress">
            <div class="indeterminate"></div>
        </div>
        <div id="empTable">
            {{-- results from AJAX--}}
        </div>
    </div>
    {{-- @if($isCurrent)
         <th><a id="hire_date" href="#" onclick="setOrder('hire_date');"><span
                         class="hide material-icons">keyboard_arrow_up</span>
                 Department Hire date</a></th>
     @else
         <th><a id="from_date" href="#" onclick="setOrder('from_date');"><span
                         class="hide material-icons">keyboard_arrow_up</span>
                 From</a></th>
         <th><a id="to_date" href="#" onclick="setOrder('to_date');"><span
                         class="hide material-icons">keyboard_arrow_up</span>
                 To</a></th>
     @endif
 </tr>
 </thead>


 <script>
     $(document).ready(function () {
         @if($order === 'asc')
         $('#{{$orderCol}} .material-icons').text('keyboard_arrow_up');
         @else
         $('#{{$orderCol}} .material-icons').text('keyboard_arrow_down');
         @endif
         $('#{{$orderCol}} .material-icons').removeClass('hide');
     });
 </script>
 <tbody>

 @if(!$isCurrent)
     @foreach($result as $emp )
         <tr>
             <td><a href="{{route('userInfo',['id' => $emp->emp_no]) }}">{{$emp->emp_no}}</a></td>
             <td><p>{{$emp->first_name}}</p></td>
             <td><p>{{$emp->last_name}}</p></td>
             <td><p>{{$emp->gender}}</p></td>
             @if($orderCol !== 'salary'&& $orderCol !== 'from_date' && $orderCol !== 'to_date')
                 <td>
                     @foreach($emp->salaries()->cursor() as $sal )
                         <div>{{$sal->salary}} zł</div>
                     @endforeach
                 </td>
                 <td>
                     @foreach($emp->salaries()->cursor() as $sal )
                         <div>{{$sal->from_date}}</div>
                     @endforeach
                 </td>
                 <td>
                     @foreach($emp->salaries()->cursor() as $sal )
                         <div>{{$sal->to_date}}</div>
                     @endforeach
                 </td>
             @else
                 <td><p>{{$emp->salary}}</p></td>  --}}{{----}}{{--
                 <td><p>{{$emp->from_date}}</p></td>
                 <td><p>{{$emp->to_date}}</p></td>
             @endif
         </tr>
     @endforeach

 @else

     @foreach($result as $emp)
         <tr>
             <td><a href="{{route('userInfo',['id' => $emp->emp_no]) }}">{{$emp->emp_no}}</a>
             </td>
             <td><p>{{$emp->first_name}}</p></td>
             <td><p>{{$emp->last_name}}</p></td>
             <td><p>{{$emp->gender}}</p></td>

             @if($tableType)
                 @if($emp->salaryNow() ===null)
                     <td><p>Fired</p></td>
                 @else
                     <td><p>{{$emp->salaryNow()->salary}} zł</p></td>
                 @endif
             @else
                 <td><p>{{$emp->salary}} zł</p></td>
             @endif

             <td>
                 <p>{{$emp->hire_date}}</p>
             </td>
         </tr>
     @endforeach
 @endif

 </tbody>
</table>

--}}               {{-- {{$result->appends($params)->links()}}
            @endif--}}

    <script>
        var filterForm = $('#sortForm');
        var empPLoader = $('#empPLoader');
        var empTable = $('#empTable');

        $(document).ready(function () {
            getTable();


            filterForm.submit(function (event) {
                event.preventDefault();
                getTable()
            });

            $(document).on('click', '.pagination a', function (e) {
                empPLoader.show();
                e.preventDefault();
                // console.log($(this).attr('href')/*.split('page=')[1]*/);
                axios.get($(this).attr('href')).then(function (response) { //todo
                    empPLoader.hide();
                    empTable.html(response.data);

                }).catch();
            });

        });

        function getTable() {
            empPLoader.show();
            console.log('loading...');
            var result = {};
            $.each(filterForm.serializeArray(), function () {
                result[this.name] = this.value;
            });

            axios.get("{{route('depTable')}}", {params: result}).then(function (response) { //todo
                empPLoader.hide();
                empTable.html(response.data);

            }).catch();
        }
    </script>


    {{--<div class="col m12 l6">--}}
    {{--<canvas id="myChart"></canvas>--}}
    {{--<script>--}}
    {{--var ctx = document.getElementById("myChart").getContext('2d');--}}
    {{--var myLineChart = new Chart(ctx, {--}}
    {{--type: 'line',--}}
    {{--data: data,--}}
    {{--options: options--}}
    {{--});--}}
    {{--</script>--}}
    {{--</div>--}}
    {{--</div>--}}





    <div class="col m12 l6">
        <h4>Managers</h4>
        <table>
            <thead>
            <tr>
                <th>Emp_no</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            </thead>

            <tbody>
            @foreach($man as $m)
                <tr>
                    <td>{{$m->emp_no}}</td>
                    <td>{{$m->first_name}}</td>
                    <td>{{$m->last_name}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    </div>





    <div class="container">
        <div class="row">
            <div class="col m12 l12 s12">
                <canvas id="myChart"></canvas>
            </div>
        </div>
    </div>


    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($dep as $d)
                        "{{$d->dept_name}}",
                    @endforeach
                ],
                datasets: [{
                    label: '# of employees',
                    data: [@foreach($dep as $d)
                        "{{$d->employees()->count()}}",
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(194,24,91, 0.2)',
                        'rgba(30,136,229, 0.2)',
                        'rgba(67,160,71, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(194,24,91, 1)',
                        'rgba(30,136,229, 1)',
                        'rgba(67,160,71,1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

    </script>


@endsection

