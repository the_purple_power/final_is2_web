@extends('layouts.main')
@section('content')
    @php
        $par = collect($params);
        $isCurrent =  collect($params)->get('current');

    @endphp
    <div class="row">
        <div class="col m6 sm12">
            <div class="input-field col s12 m6">
                {{--<form name="add" action="{{route('department')}}" method="get">--}}
                <select name="title" onchange="this.form.submit()" form="sortForm">
                    @if($par->get('department') ===null)
                    <option value="" disabled selected>Choose title</option>
                    @else
                        <option value="{{$par->get('title')}}" selected>{{$par->get('title')}}</option>

                        @endif


                        <option value="Assistant Engineer">Assistant Engineer</option>
                        <option value="Engineer">Engineer</option>
                        <option value="Manager">Manager</option>
                        <option value="Senior Engineer">Senior Engineer</option>
                        <option value="Senior Staff">Senior Staff</option>
                        <option value="Staff">Staff</option>
                        <option value="Technique Leader">Technique Leader</option>

                </select>
                {{--</form>--}}
            </div>
        <div class="col m6 s12">
            <h5>Current status</h5>
            <div class="switch">
                <label>
                    Off
                    <input type="checkbox" onclick="sortForm.submit();" name="current" form="sortForm"
                           @if($isCurrent)
                           checked
                            @endif
                    >
                    <span class="lever"></span>
                    On
                </label>
            </div>
        </div>
        </div>
    </div>


    <div class="row center-align">
        <div class="col m12 l12">


            <div id="empPLoader" class="progress">
                <div class="indeterminate"></div>
            </div>
            <div id="empTable">

                {{-- results from AJAX--}}
            </div>
        </div>
        <script>
            var filterForm = $('#sortForm');
            var empPLoader = $('#empPLoader');
            var empTable = $('#empTable');

            $(document).ready(function () {
                getTable();


                filterForm.submit(function (event) {
                    event.preventDefault();
                    getTable()
                });

                $(document).on('click', '.pagination a', function (e) {
                    empPLoader.show();
                    e.preventDefault();
                    // console.log($(this).attr('href')/*.split('page=')[1]*/);
                    axios.get($(this).attr('href')).then(function (response) { //todo
                        empPLoader.hide();
                        empTable.html(response.data);

                    }).catch();
                });

            });

            function getTable() {
                empPLoader.show();
                console.log('loading...');
                var result = {};
                $.each(filterForm.serializeArray(), function () {
                    result[this.name] = this.value;
                });

                axios.get("{{route('titleTable')}}", {params: result}).then(function (response) { //todo
                    empPLoader.hide();
                    empTable.html(response.data);

                }).catch();
            }
        </script>

        {{--<div class="row center-align">
            <div class="col m12 l6">--}}

        {{--<table class=" centered">
            <thead>
            <tr>

                <th><p>Emp_no/info</p></th>
                <th><p>First Name</p></th>
                <th><p>Salary</p></th>
                <th><p>From date</p></th>
                <th><p>To date</p></th>

            </tr>
            </thead>

            <tbody>
            @foreach($result as $emp)
                <tr>
                    <td><a href="{{route('userInfo',$emp->emp_no)}}">{{$emp->emp_no}}</a></td>
                    <td><p>{{$emp->first_name}}</p></td>
                    --}}{{--<td>--}}{{--
                            --}}{{--@foreach($emp->salary as $s)--}}{{--
                                --}}{{--<p>{{$s->salary}}</p>--}}{{--
                                --}}{{--@endforeach--}}{{--
                        --}}{{--</td>--}}{{--
                    --}}{{--<td>--}}{{--
                        --}}{{--@foreach($emp->salary as $s)--}}{{--
                            --}}{{--<p>{{$s->from_date}}</p>--}}{{--
                        --}}{{--@endforeach--}}{{--
                    --}}{{--</td>--}}{{--
                    --}}{{--<td>--}}{{--
                        --}}{{--@foreach($emp->salary as $s)--}}{{--
                            --}}{{--<p>{{$s->to_date}}</p>--}}{{--
                            --}}{{--@endforeach--}}{{--
                        --}}{{--</td>--}}{{--
                    <td>{{$emp->salaries()->first()->salary}}</td>
                    <td>{{$emp->from_date}}</td>
                    <td>{{$emp->to_date}}</td>
                </tr>
            @endforeach


            </tbody>
        </table>
        </div>--}}
        <div class="col m12 l6">
            <canvas id="myChart"></canvas>
            <script>
                var ctx = document.getElementById("myChart").getContext('2d');
                var myLineChart = new Chart(ctx, {
                    type: 'line',
                    data: data,
                    options: options
                });
            </script>
        </div>
    </div>


    <div class="col m12 l6 s12">
        <canvas id="myChart"></canvas>

          </div>
    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

    </script>


@endsection

