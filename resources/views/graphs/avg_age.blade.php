@extends('index')
@section('avg_age')
    <div class="section">
        <h2 class="center">Average salary by age</h2>
        <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
            <canvas id="avg_age"></canvas>
        </div>
    </div>
    {{--Avg by age--}}
    <script>
        var ctx = $("#avg_age");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($avg_age as $aa)
                        '{{ $aa['age'] }}',
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($avg_age as $aa)
                        {{ $aa['salary'] }},
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(67,160,71,0.05)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(194,24,91, 0.2)',
                        'rgba(238,255,65,0.2)',
                        'rgba(118,255,3,0.2)',
                        'rgba(224,64,251,0.2)',
                        'rgba(100,181,246,0.2)',
                        'rgba(0,121,107,0.2)',
                        'rgba(220,231,117,0.2)',
                        'rgba(255,145,0,0.2)'
                    ],
                    borderColor: [
                        'rgb(67,160,71)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 206, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(153, 102, 255)',
                        'rgb(255, 159, 64)',
                        'rgb(194,24,91)',
                        'rgb(238,255,65)',
                        'rgb(118,255,3)',
                        'rgb(224,64,251)',
                        'rgb(100,181,246)',
                        'rgb(0,121,107)',
                        'rgb(220,231,117)',
                        'rgb(255,145,0)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });
    </script>
@endsection