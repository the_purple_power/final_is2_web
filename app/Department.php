<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $primaryKey = 'dept_no';
    public $incrementing = false;

    public function employees(){
//        return $this->belongsToMany('App\Employee','dept_emp')->withPivot('emp_no','dept_no');
        return $this->belongsToMany('App\Employee','dept_emp','dept_no','emp_no');

    }

        public function managers(){
            return $this->belongsToMany(Employee::class,'employee','dept_no','emp_no')->withPivot('to_date');
        }

}
