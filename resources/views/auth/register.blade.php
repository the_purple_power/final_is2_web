@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="email" value="placeholder@mail.com">
                        <div class="input-field">
                            <input id="empID" name="empID" type="text">
                            <label for="empID">Employee ID</label>
                        </div>
                        <div class="input-field">
                            <input id="password" name="password" type="password">
                            <label for="password">Password</label>
                        </div>

                        <div class="input-field">
                            <input id="password-confirm" name="password_confirmation" type="password">
                            <label for="password-confirm">Confirm Password</label>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
