<?php

namespace App\Http\Controllers;

use App\Dept_Emp;
use App\Employee;
use App\Salary;
use App\Title;
use App\Current_Dept_Emp;
use Illuminate\Http\Request;

class IndexGraphsController extends Controller
{
    public function index(Request $request = null)
    {
        $params = $request->query->all();
        return view('index',
            compact('params'));
    }


    public function fluidity()
    {
        $params = null;

        // Num of people hired each year HAS INDEX
        $hired = Employee::selectRaw('count(*) as employees, year(hire_date) as year')
            ->groupBy('year')
            ->orderBy('year')
            ->get();
        $hired = $hired->toArray();

        // Fired people HAS INDEX
        $fired = Dept_Emp::selectRaw('count(*) as fired, year(to_date) as year')
            ->where('to_date', '<', date('Y-m-d', time()))
            ->groupBy('year')
            ->orderBy('year')
            ->get();
        $fired = $fired->toArray();

        return view('graphs.fluidity', compact('params','hired', 'fired'));
    }


    public function numDept()
    {
        $params = null;

        // Num by department
        $num_dept = Current_Dept_Emp::join('departments', 'current_dept_emp.dept_no', '=', 'departments.dept_no')
            ->selectRaw('count(current_dept_emp.emp_no) as total, departments.dept_name as department_name')
            ->groupBy('current_dept_emp.dept_no')
            ->get();
        $num_dept = $num_dept->toArray();
        return view('graphs.num_depts', compact('params','num_dept'));
    }
    public function avgYear()
    {

        $params = null;

        // Avg salary by year
        $avg_year = Salary::selectRaw('year(to_date) as year, avg(salary) as salary')
            ->groupBy('year')
            ->get();
        $avg_year = $avg_year->toArray();
        return view('graphs.avg_year', compact('params','avg_year'));
    }

    public function avgGender()
    {
        $params = null;
        // Avg salary by gender
        $avg_gender = Employee::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')
            ->selectRaw('employees.gender as gender, avg(salaries.salary) as salary')
            ->groupBy('gender')
            ->get();
        $avg_gender = $avg_gender->toArray();

        return view('graphs.avg_gender', compact('params','avg_gender'));
    }

    public function avgAge()
    {
        $params = null;
        // Avg by age
        $avg_age = Employee::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')
            ->selectRaw('YEAR(CURRENT_TIMESTAMP) - YEAR(employees.birth_date) - (RIGHT(CURRENT_TIMESTAMP, 5) < RIGHT(employees.birth_date, 5)) as age, avg(salaries.salary) as salary')
            ->groupBy('age')
            ->get();
        $avg_age = $avg_age->toArray();
        return view('graphs.avg_age', compact('params','avg_age'));
    }

    public function avgTitle()
    {
        $params = null;
        // Avg salary by titles
        $avg_title = Title::join('salaries', 'salaries.emp_no', '=', 'titles.emp_no')
            ->selectRaw('titles.title as title, avg(salaries.salary) as salary')
            ->groupBy('titles.title')
            ->get();
        $avg_title = $avg_title->toArray();
        return view('graphs.avg_title', compact('params','avg_title'));
    }

    public function numGenders() {
        $params = null;
        $num_genders = Employee::selectRaw('count(*) as total, gender')
            ->groupBy('gender')
            ->get();
        $num_genders = $num_genders -> toArray();
        return view('graphs.num_genders', compact('params', 'num_genders'));
    }

}
