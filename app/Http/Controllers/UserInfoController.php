<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Salary;
use App\Title;
use Illuminate\Http\Request;

class UserInfoController extends Controller {
    public function userInfo($id,Request $request) {
        $emp = (new Employee)->find($id);

        $salary = (new Salary)->where('emp_no',$id)->get();

        $params = $request->query->all();

        $dep = (new Employee)->where('emp_no',$id)->first()->departmentNow();

        $titles = (new Title)->where('emp_no',$id)->get();
        if ($titles->last()->to_date>9000)
            $titles->last()->to_date = date('Y-m-d');
        if ($titles->first()->to_date>9000)
            $titles->first()->to_date = date('Y-m-d');

        return view('user', compact('emp','params','salary','titles','dep'));
    }
}


//Департамент
/*$depn = Dept_Emp::select('dept_no')->where('emp_no', '=', $id)->first()->dept_no;
$info[0] = Department::select('dept_name')->where('dept_no', '=', $depn)->first()->dept_name;
//Зарплата
$info[1] = Salary::select('salary')->where('emp_no', '=', $id)->first()->salary;
//Должность
$info[2] = Title::select('title')->where('emp_no', '=', $id)->first()->title;
//Менеджер
$man = Dept_Manager::select('dept_no')->where('emp_no', '=', $id)->first();
if ($man !== null) $man = "Manager"; else $man = 0;
$info[3] = $man;*/