<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model {
    protected $primaryKey = 'emp_no';

    public function departments() {
//        return $this->belongsToMany('App\Department','dept_emp')->withPivot('emp_no','dept_no');
        return $this->belongsToMany('App\Department', 'dept_emp', 'emp_no', 'dept_no');
    }

    public function departmentNow() {
        $result = $this->belongsToMany(Department::class, 'current_dept_emp', 'emp_no', 'dept_no')->withPivot('to_date')->first();
        if ($result->pivot->to_date < date('Y-m-d', time())) {
            return null;
        }
        return $result;
    }

    public function manager() {
        return $this->belongsToMany(Department::class, 'dept_manager', 'emp_no', 'dept_no')->withPivot('to_date');
    }

    public function managerNow() {
        $now = date('Y-m-d', time());
        $mng = $this->manager()->get();
        foreach ($mng as $key => $val) {
            if ($val->pivot->to_date > $now) return $mng->get($key);
        }
        return null;
    }

    public function title() {
        return $this->hasMany(Title::class,'emp_no');
    }
    public function titleNow(){
        $now = date('Y-m-d', time());
        $titles  = $this->title()->get();
        foreach($titles as $key => $val){
            if (($val->to_date) > $now) {
             return $val;
            }
        }
        return null;
    }

    public function salaries() {
        return $this->hasMany(Salary::class,'emp_no');
    }

    public function salaryNow(){
        return  $this->salaries()->where('to_date','>', date('Y-m-d', time()))->first();
    }

    public function scopeCurrentSalary($query)
    {
        return $query->where('salaries.to_date','>', date('Y-m-d', time()));
    }

public function titles(){
        return $this->hasMany(Title::class, 'emp_no');
}

}
