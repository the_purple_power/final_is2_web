@extends('index')
@section('num_depts')
    <div class="section">
        <h2 class="center">Number of people by genders</h2>
        {{--SLOW--}}
        <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
            <canvas id="num_genders"></canvas>
        </div>
    </div>
    {{--Total by dept--}}
    <script>
        var ctx = $("#num_genders");
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [
                    'Males', ' Females'
                ],
                datasets: [{
                    data: [
                        @foreach($num_genders as $nd)
                        {{ $nd['total'] }},
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(67,160,71,0.05)',
                        'rgba(54, 162, 235, 0.2)'
                    ],
                    borderColor: [
                        'rgb(67,160,71)',
                        'rgb(54, 162, 235)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: true,
                    position: 'right'
                }
            }
        });
    </script>
@endsection