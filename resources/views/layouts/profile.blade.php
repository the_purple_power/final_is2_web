<!DOCTYPE html>
<html>
<head>
    <meta name="author" content="the_purple_power">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <!-- Materialize -->
    <!--<link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.4/css/materialize.min.css">-->

    <link rel="stylesheet" href="{{asset('materialize/css/materialize.css')}}">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.4/js/materialize.min.js"></script>
    <!-- Materialize icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinFlat.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/js/ion.rangeSlider.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
</head>


<body class="row">
<nav class="deep-purple darken-2">
    <div class="nav-wrapper">
        <a href="#" data-target="slideBar" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right">
            <li><a href="#">Login</a></li>
            <li><a href="#">IDK</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col m9 offset-m1">
            <div class="card-panel blue-grey darken-1">
                <div class="card-content white-text">
                    <div class="row">
                        <div class="col m8 offset-m1">
                            <span class="card-title"><h4>Employee #</h4></span>
                        </div>
                        <div class="col m3">
                            <i class="large material-icons">account_box</i>
                        </div>
                    </div>
                    <div class="divider "></div>
                    <div class="row">
                        <div class="col m6 offset-m1">
                            <p>Name</p>

                            <p>SecondName</p>
                            <p>Salary</p>
                            <p>Department</p>
                            <p>Title</p>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>