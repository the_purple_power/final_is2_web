@extends('index')
@section('avg_title')
    <div class="section">
        <h2 class="center">Average salary by title</h2>
        {{--VERY SLOW--}}
        <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
            <canvas id="avg_title"></canvas>
        </div>
    </div>
    {{--Avg sal by title--}}
    <script>
        var ctx = $("#avg_title");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($avg_title as $ay)
                        '{{ $ay['title'] }}',
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($avg_title as $ay)
                        {{ round($ay['salary']) }},
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(67,160,71,0.05)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(194,24,91, 0.2)'
                    ],
                    borderColor: [
                        'rgb(67,160,71)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 206, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(153, 102, 255)',
                        'rgb(255, 159, 64)',
                        'rgb(194,24,91)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection