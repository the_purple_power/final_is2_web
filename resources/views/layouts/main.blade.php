<!DOCTYPE html>
<html>
<head>
    <meta name="author" content="the_purple_power">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <!-- Materialize -->
    <!--<link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.4/css/materialize.min.css">-->

    <link rel="stylesheet" href="{{asset('materialize/css/materialize.css')}}">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.4/js/materialize.min.js"></script>
    <!-- Materialize icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinFlat.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/js/ion.rangeSlider.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>

<body>
<nav class="deep-purple darken-2">
    <div class="nav-wrapper">
        <a href="#" data-target="mobileNav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        @if(Request::path() !== '/' )
            <ul class="left">
                <li><a href="#" data-target="sideBar" class="sidenav-trigger show-on-large"><i
                                class="material-icons">chevron_right</i></a>
                </li>
            </ul>
        @endif
        <ul class="left hide-on-med-and-down">
            <li><a href="{{route('employee')}}">Employee</a></li>
            <li><a href="{{route('title')}}">Title</a></li>
            <li><a href="{{route('department')}}">Department</a></li>


        </ul>
        <!--dropdown -->

        <ul class="right">
            @if(Auth::guest())
            <li><a href="{{route('login')}}">Login</a></li>
                <li><a href="{{route('register')}}">Register</a></li>
                @else
                <li><a href="{{url('employee/' . Auth::user()->empID)}}">{{Auth::user()->name}}</a></li>
            <li><a href="{{route('logout')}}"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                @endif
        </ul>


    </div>
</nav>

<ul class="sidenav" id="mobileNav">
    <ul>
        <li><a href="{{route('employee')}}">Employee</a></li>
        <li><a href="{{route('title')}}">Title</a></li>
        <li><a href="#">Department</a></li>
    </ul>
</ul>
@php $par = collect($params); @endphp
@if(Request::path() !== '/')
    <form id="sortForm" class="row" method="get" action="{{URL::full()}}">
        <!-- salary=3000%3B8000&dateFrom=16-04-2018&dateTo=25-04-2018 -->
        <input type="hidden" name="department" value="{{$par->get('department')}}">

        <ul id="sideBar" class="sidenav col s10 m6 l3 collapsible expandable">
            <li><a href="#" id="logo" class="sidenav-close brand-logo center-align material-icons">close</a></li>
            <li>
                <div class="divider"></div>
            </li>
            <li>
                <button class="btn col s12" type="submit">submit</button>
            </li>

            <li>
                <div class="collapsible-header">Salary</div>
                <div class="collapsible-body">

                    <div style="padding:10px 10px; height:50px;">
                        <input name="salary" id="salarySlider" value="{{$par->get('salary')}}">
                        <!-- input will return  $min;$max (3000;8000) -->
                    </div>


                </div>
            </li>

            <li>
                <div class="collapsible-header">Period</div>
                <div class="collapsible-body">
                    <div class="">
                        <p>From</p>
                        <input name="dateFrom" id="dateFrom" type="text" class="datepicker"
                               value="{{$par->get('dateFrom')}}">
                    </div>
                    <div>
                        <p>To</p>
                        <input name="dateTo" id="dateTo" type="text" class="datepicker" value="{{$par->get('dateTo')}}">
                    </div>
                </div>
            </li>

            <li>
                <div class="collapsible-header">Gender</div>
                <div class="collapsible-body">
                    <p>
                        <label>
                            <input name="male" id="dateFrom" type="checkbox" class="filled-in"
                                   @if($par->get('male'))
                                   checked
                                    @endif
                            >
                            <span>M</span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input name="female" id="dateTo" type="checkbox" class="filled-in"
                                   @if($par->get('female'))
                                   checked
                                    @endif>
                            <span>F</span>
                        </label>
                    </p>
                </div>
            </li>


        </ul>
        {{--value will be set in view--}}
        <input id="orderColumn" type="text" style="display: none;" name="orderColumn" value="{{$orderCol or ''}}">
        <input id="sortOrder" type="text" style="display: none;" name="sortOrder" value="{{$order or ''}}">
    </form>
@endif

<div id="main">
    @yield('content')
</div>
</body>
@php
    $salaryRange = $par->get('salary');
        $divPos = strpos($salaryRange, ';');
        $salaryMin = substr($salaryRange, '0', $divPos);
        $salaryMax = substr($salaryRange, $divPos + 1);
@endphp
<script>

    $(document).ready(function () {
        $('#sortForm').submit(function () {
            console.log($('#sortForm').serialize());
        });
        $('.modal').modal();
        $('.sidenav').sidenav();
        $('.collapsible').collapsible({accordion: false});
        $('.datepicker').datepicker({
            container: 'body',
            format: 'yyyy-mm-dd',
            yearRange: [1985, 2000]
        });


    });

    $('#salarySlider').ionRangeSlider({
        type: 'integer',
        min: 0,
        max: 160000,
        from: '{{$salaryMin or 1000}}',
        to: '{{$salaryMax or 150000}}',
        step: 10000,
        grid: true,
        prettify_enabled: true,
        prefix: '$'
    });

    $('.dropdown-trigger').dropdown({
        hover:true,
        isScrollable:true
    });

</script>
</html>