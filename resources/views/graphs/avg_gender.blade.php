@extends('index')
@section('avg_gender')
    <div class="section">
        <h2 class="center">Average salary by gender</h2>
        {{--VERY SLOW--}}
        <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
            <canvas id="avg_gender"></canvas>
        </div>
    </div>
    {{--Avg by gender--}}
    <script>
        var ctx = $("#avg_gender");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Males', 'Females'],
                datasets: [{
                    data: [
                        @foreach($avg_gender as $ay)
                        {{ round($ay['salary']) }},
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(67,160,71,0.05)',
                        'rgba(54, 162, 235, 0.2)'
                    ],
                    borderColor: [
                        'rgb(67,160,71)',
                        'rgb(54, 162, 235)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection