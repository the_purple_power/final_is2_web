<?php

namespace App\Http\Controllers;

use App\Department;
use App\Dept_Manager;
use Illuminate\Http\Request;
use App\Employee;
use App\Salary;

class DepController extends Controller {
    public function refresh(Request $request) {
        if (empty($request->all())) { //default
            $request->request->add(["salary" => "0;160000", "dateFrom" => null, "dateTo" => null, "male" => "on", "female" => "on", "orderColumn" => "emp_no", "sortOrder" => "asc","department"=>null,'current'=>'on']);
        }
        $params = $request->query->all();


        $dep = Department::all();

        $salaryRange = $request->salary;
        $divPos = strpos($salaryRange, ';');
        $salaryMin = substr($salaryRange, '0', $divPos);
        $salaryMax = substr($salaryRange, $divPos + 1);

        if (!($request->orderColumn === null || $request->sortOrder === null)) {
            $orderCol = $request->orderColumn;
            $order = $request->sortOrder;
        } else {
            $orderCol = 'employees.emp_no';
            $order = 'asc';
        }

        $tableType = ($orderCol !== 'salary' && $orderCol !== 'from_date' && $orderCol !== 'to_date' && ($salaryMin == 0 && $salaryMax == 160000));
        //$tableType: true-Employee; false - Salary
        // dont sort and default salary - true , otherwise - false

        if ($request->dateFrom === null) $fromDate = '1984-12-12'; else $fromDate = $request->dateFrom;

        if ($request->dateTo === null) $toDate = date('Y-m-d', time()); else $toDate = $request->dateTo;

        if ($request->department == null) {
            $result = (new Employee);

        } else if ($salaryMax == 160000 && $salaryMin==0 && $request->from_date == null && $request->to_date == null) {
            $result = (new Department)->where('dept_name', $request->department)->first()->employees();
            //$result = (new Employee)->join('salaries', 'salaries.emp_no', '=', 'employees.emp_no')->join('dept_emp', 'dept_emp.emp_no', '=', 'employees.emp_no')->join('departments', 'dept_emp.dept_no', 'departments.dept_no');
            //$result = $result->where('department.dept_name',$request->department)->first()->employees();
        } else {

//            $result = (new Salary)->join('employees', 'salaries.emp_no', '=', 'employees.emp_no')->join('dept_emp', 'dept_emp.emp_no', '=', 'employees.emp_no')->join('departments', 'dept_emp.dept_no', 'departments.dept_no');
            $result = (new Employee)->join('salaries', 'salaries.emp_no', '=', 'employees.emp_no')->join('dept_emp', 'dept_emp.emp_no', '=', 'employees.emp_no')->join('departments', 'dept_emp.dept_no', 'departments.dept_no');

            $result = $result->whereBetween('salary', [$salaryMin, $salaryMax])->where('salaries.from_date', '>', $fromDate)->where('salaries.to_date', '<', $toDate)->where('departments.dept_name', $request->department);
        }

        if ($request->current === 'on' && !$tableType) {
            $result = $result->whereDate('salaries.to_date', '>', '9998-01-01');
        }
        if ($request->male === 'on' && $request->female === null) {
            $result = $result->where('gender', '=', 'M');

        } else if ($request->female === 'on' && $request->male === null) {
            $result = $result->where('gender', '=', 'F');
        }

        if ($orderCol === 'emp_no') {
            $result = $result->orderBy('employees.' . $orderCol, $order)->paginate(6);
        } else if ($orderCol === 'salary' && $salaryMax == 160000 && $salaryMin==0 || $orderCol ==='from_date' && $fromDate = '1984-12-12') {

            $result = (new Employee)->join('salaries', 'salaries.emp_no', '=', 'employees.emp_no')->join('dept_emp', 'dept_emp.emp_no', '=', 'employees.emp_no')->join('departments', 'dept_emp.dept_no', 'departments.dept_no');
            $result = $result->orderBy('salaries.'. $orderCol,$order)->paginate(6);
        }
        else {

            $result = $result->orderBy($orderCol, $order)->paginate(6);
        }
        if ($request->current === 'on') {
            $isCurrent = true;
        } else
            $isCurrent = false;

        if ($request->department!==null) {
            if ($request->current!=='on')
            //$man = (new Department)->where('dept_name', $request->department)->first()->managers();
            $man = (new Department)->join('dept_manager','dept_manager.dept_no','=','departments.dept_no')->join('employees','employees.emp_no','=','dept_manager.emp_no')->where('departments.dept_name',$request->department)->get();
            else
                $man = (new Department)->join('dept_manager','dept_manager.dept_no','=','departments.dept_no')->join('employees','employees.emp_no','=','dept_manager.emp_no')->where('departments.dept_name',$request->department)->where('dept_manager.to_date','>','9998-01-01')->get();

        }
        else {
            if ($request->current!=='on')
            $man = (new Department)->join('dept_manager','dept_manager.dept_no','=','departments.dept_no')->join('employees','employees.emp_no','=','dept_manager.emp_no')->get();
            else $man = (new Department)->join('dept_manager','dept_manager.dept_no','=','departments.dept_no')->join('employees','employees.emp_no','=','dept_manager.emp_no')->where('dept_manager.to_date','>','9998-01-01')->get();

        }
     // else
//        return compact('result', 'params', 'dep', 'tableType', 'order', 'orderCol','man');


//        return view('department', compact('result', 'params', 'dep', 'tableType', 'order', 'orderCol'));
        return  compact('result', 'params', 'dep', 'tableType', 'order', 'orderCol','isCurrent','man');
    }
    public function get(Request $request) {
        return view('tables.depTable', $this->refresh($request))->render();
    }

    public function show(Request $request) {
        return view('department', $this->refresh($request));
    }
}
