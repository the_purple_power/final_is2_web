<?php

namespace App\Http\Controllers;

use App\Title;
use Illuminate\Http\Request;
use App\Employee;

class TitleController extends Controller {
    public function findAndSort(Request $request = null) {
        if (empty($request->all())) { //default
            $request->request->add(["salary" => "0;160000", "dateFrom" => null, "dateTo" => null, "male" => "on", "female" => "on", "orderColumn" => "emp_no", "sortOrder" => "asc", "current" => "on", 'emp' => null]);
        }
        $salaryRange = $request->salary;
        $divPos = strpos($salaryRange, ';');
        $salaryMin = substr($salaryRange, '0', $divPos);
        $salaryMax = substr($salaryRange, $divPos + 1);

        if ($request->dateFrom === null) $fromDate = '1984-12-12'; else $fromDate = $request->dateFrom;

        if ($request->dateTo === null) $toDate = '9999-12-12'; else $toDate = $request->dateTo;


        if (!($request->orderColumn === null || $request->sortOrder === null)) {
            $orderCol = $request->orderColumn;
            $order = $request->sortOrder;
        } else {
            $orderCol = 'employees.emp_no';
            $order = 'asc';
        }

        $tableType = ($orderCol !== 'salary' && $orderCol !== 'from_date' && $orderCol !== 'to_date' && ($salaryMin == 0 && $salaryMax == 160000));
        //$tableType: true-Employee; false - Salary

        if ($tableType) {
            $result = (new Employee)->join('titles', 'titles.emp_no', '=', 'employees.emp_no');
            if ($fromDate !== null || $toDate !== null) {
                $result = $result->whereBetween('hire_date', [$fromDate, $toDate]);
            }
        } else {
            $result = (new Employee)->join('titles', 'titles.emp_no', '=', 'employees.emp_no');
            $result = $result->where('from_date', '>', $fromDate)->where('to_date', '<', $toDate);
            if ($request->current === 'on' && $tableType) $result = $result->whereBetween('hire_date', [$fromDate, $toDate]);
        }
        if ($request->male === 'on' && $request->female === null) $result = $result->where('gender', '=', 'M');
        if ($request->female === 'on' && $request->male === null) $result = $result->where('gender', '=', 'F');


        if ($request->current === 'on' && !$tableType) {
            $result = $result->whereDate('to_date', '>', '9998-01-01');
        }

        if ($request->emp !== null) {
            $result = $result->where('first_name', 'LIKE', "%$request->emp%")->where('last_name', 'LIKE', "%$request->emp%");
        }

        if ($request->title !== null) {
            $result = $result->where('title', '=', $request->title);
        }

        if ($orderCol === 'emp_no') {
            $result = $result->orderBy('employees.' . $orderCol, $order)->paginate(6);
        } else
            $result = $result->orderBy($orderCol, $order)->paginate(6);

        $params = $request->query->all();
        if ($request->current === 'on') {
            $isCurrent = true;
        } else
            $isCurrent = false;


//        dd($result->first());
        return compact('result', 'orderCol', 'order', 'params', 'tableType', 'isCurrent');
//        return view('employee', compact('result', 'orderCol', 'order', 'params', 'tableType'));
    }

    public function get(Request $request) {
        return view('tables.titleTable', $this->findAndSort($request))->render();
    }

    public function show(Request $request) {
        return view('title', $this->findAndSort($request));
    }

}
