<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titles2', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_no');
            $table->string('title');
            $table->date('from_date');
            $table->date('to_date');
        });
        DB::statement("INSERT INTO titles2 (emp_no,title,from_date,to_date) SELECT emp_no,title,from_date,to_date FROM titles;");
        Schema::dropIfExists('titles');
        Schema::rename('titles2', 'titles');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('title2');
    }
}
