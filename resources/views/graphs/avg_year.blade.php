@extends('index')
@section('avg_year')
    <div class="section">
        <h2 class="center">Average salary by year</h2>
        {{--SLOW--}}
        <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
            <canvas id="avg_year"></canvas>
        </div>
    </div>

    {{--Avg sal by year--}}

    <script>
        var ctx = $("#avg_year");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach($avg_year as $ay)
                    {{ $ay['year'] }},
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($avg_year as $ay)
                        {{ $ay['salary'] }},
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(67,160,71,0.05)'
                    ],
                    borderColor: [
                        'rgb(67,160,71)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection