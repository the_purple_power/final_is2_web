<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    protected $table = 'titles';

    public function employees() {
        return $this->hasMany(Employee::class, 'emp_no','emp_no');
    }

    public function salaries() {
        return $this->hasMany(Salary::class);
    }
//    public function avgSalaries() {
//        return $this->salaries()
//            ->selectRaw('avg(salary) as salary, title')
//            ->groupBy('title');
//    }
//    public function getAvgSalariesAttribute() {
//        if ( ! array_key_exists('avgSalaries', $this->relations)) {
//            $this->load('avgSalaries');
//        }
//
//        $relation = $this->getRelation('avgSalaries')->get();
//
//        return ($relation) ? $relation->aggregate : null;
//    }
}
