@extends('index')
@section('fluidity')
    <div class="section">
        <h2 class="center">Number of hired people per year</h2>
        {{--FAST--}}
        <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
            <canvas id="hired"></canvas>
        </div>
    </div>
    <div class="section">
        <h2 class="center">Number of fired people per year</h2>
        {{--FAST--}}
        <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
            <canvas id="fired"></canvas>
        </div>
    </div>

    {{--Num of hired--}}
    <script>

        var ctx = $("#hired");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach($hired as $h)
                    {{ $h['year'] }},
                    @endforeach
                ],
                datasets: [{
                    // label: 'Number of people hired by each year since 1985',
                    data: [
                        @foreach($hired as $h)
                        {{ $h['employees'] }},
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>

    {{--Num of fired--}}
    <script>

        var ctx = $("#fired");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach($fired as $h)
                    {{ $h['year'] }},
                    @endforeach
                ],
                datasets: [{
                    // label: 'Number of people hired by each year since 1985',
                    data: [
                        @foreach($fired as $h)
                        {{ $h['fired'] }},
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection