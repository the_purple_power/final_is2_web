@extends('layouts.main')
@section('content')
    <div class="row center-align">
        <h3>Choose statistics type:</h3>
        <div class="container">
            <div class="row">
                <div class="col l3 hide-on-med-and-down"></div>
                <div class="col l6 s12">
                    <div class="row center-block">
                        <div class="col s12 l4 my1"><a href="#" class="btn">Employee</a></div>
                        <div class="col s12 l4 my1"><a href="#" class="btn">Title</a></div>
                        <div class="col s12 l4 my1"><a href="#" class="btn">Department</a></div>
                    </div>
                </div>
                <div class="col l3 hide-on-med-and-down"></div>
            </div>
        </div>
    </div>
    <div class="container center">
        <h3 class="">Graphics</h3>
        <div class="container">
            <div class="row">
                <div class="col l3 hide-on-med-and-down"></div>
                <div class="col l6 s12">
                    <div class="row center-block">
                        <a class="btn waves-effect waves-light my1" href="{{ route('fluidity') }}">Fluidity
                            <i class="material-icons right">send</i>
                        </a>
                        <a class="btn waves-effect waves-light my1" href="{{ route('num_depts') }}">Departments
                            <i class="material-icons right">send</i>
                        </a>
                        <a class="btn waves-effect waves-light my1" href="{{ route('num_genders') }}">Total by genders
                            <i class="material-icons right">send</i>
                        </a>

                        <a class='dropdown-trigger btn' href='#' data-target='types'>Average Salary by</a>
                        <ul id='types' class='dropdown-content'>
                            <li><a href="{{ route('avg_age') }}">Age</a></li>
                            <li><a href="{{ route('avg_gender') }}">Gender</a></li>
                            <li class="divider" tabindex="-1"></li>
                            <li><a href="{{ route('avg_title') }}">Title</a></li>
                            <li><a href="{{ route('avg_year') }}">Year</a></li>
                        </ul>

                    </div>
                </div>
                <div class="col l3 hide-on-med-and-down"></div>
            </div>
        </div>
    </div>
    <div class="container">
        @yield('avg_age')
        @yield('avg_gender')
        @yield('avg_title')
        @yield('avg_year')
        @yield('fluidity')
        @yield('num_depts')
    </div>
    <script>
        $('.dropdown-trigger').dropdown();
    </script>
@endsection