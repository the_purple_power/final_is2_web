<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dept_Manager extends Model
{
    protected $table = "dept_manager";
    protected $primaryKey = 'emp_no';
    public $incrementing = false;

}
