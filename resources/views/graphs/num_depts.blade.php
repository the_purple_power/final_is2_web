@extends('index')
@section('num_depts')
    <div class="section">
        <h2 class="center">Number of people by department</h2>
        {{--SLOW--}}
        <div class="chart-container" style="position: relative; height:60vh; width:60vw;">
            <canvas id="num_dept"></canvas>
        </div>
    </div>
    {{--Total by dept--}}
    <script>
        var ctx = $("#num_dept");
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [
                    @foreach($num_dept as $nd)
                        '{{ $nd['department_name'] }}',
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($num_dept as $nd)
                        {{ $nd['total'] }},
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(67,160,71,0.05)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(194,24,91, 0.2)',
                        'rgba(238,255,65,0.2)',
                        'rgba(118,255,3,0.2)'
                    ],
                    borderColor: [
                        'rgb(67,160,71)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 206, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(153, 102, 255)',
                        'rgb(255, 159, 64)',
                        'rgb(194,24,91)',
                        'rgb(238,255,65)',
                        'rgb(118,255,3)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: true,
                    position: 'right'
                }
            }
        });
    </script>
@endsection